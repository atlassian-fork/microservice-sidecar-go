package main

import (
	"net/http"

	"log"

	proxyHttp "bitbucket.org/atlassian/microservice-sidecar-go/pkg/http"
	"bitbucket.org/atlassian/microservice-sidecar-go/pkg/swagger"
	"github.com/spf13/viper"
)

func main() {
	proxyHandler, err := proxyHttp.NewProxy(viper.GetViper())
	if err != nil {
		log.Fatal("Could not create proxy handler", err)
	}

	swaggerMiddleware, error := swagger.NewMiddleware(proxyHandler, viper.GetViper())
	if error != nil {
		log.Fatal("Could not create swagger middleware", error)
	}

	s := &http.Server{
		Addr:    ":8080",
		Handler: swaggerMiddleware,
	}

	serverError := s.ListenAndServe()
	if serverError != nil {
		log.Fatal(serverError)
	}
}

func init() {
	viper.AutomaticEnv()
	error := viper.BindEnv("swaggerFilePath", "SIDECAR_SWAGGER_FILE")
	if error != nil {
		log.Fatal(error)
	}
	err := viper.BindEnv("proxyTargetUrl", "PROXY_TARGET_URL")
	if err != nil {
		log.Fatal(err)
	}
	viper.Set("proxyTargetUrl", "http://localhost:8081")
}
